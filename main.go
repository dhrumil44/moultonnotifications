package main

import (
	"database/sql"
	"flag"
	"fmt"
	"io"
	"log"
	"net/smtp"
	"os"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

// interval determines the frequency of email logs
const (
	interval = time.Minute * 10
)

// EMAILPW --
var (
	EMAILPW string
	TOEMAIL string
)

func main() {

	flag.StringVar(&EMAILPW, "emailpw", "Ag3leBTS.com", "password for 'from' email")
	flag.StringVar(&TOEMAIL, "toemail", "lw@defenage.com", "destination email for log notifications")
	flag.Parse()
	// start logger - will create a new log file each day
	go func() {
		for {
			f := setLogFile()
			time.Sleep(time.Hour * 24)
			f.Close()
		}
	}()

	// Connect to DB - defer close for progam end
	db, err := openDB()
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()
	log.Println("DB Connected")

	// listen for errors from the main processes
	errChan := make(chan error)
	errCount := 0
	go func() {
		for err := range errChan {
			log.Printf("Error # %d:\n\t%s\n", errCount, err.Error())
			errCount++

			if errCount%10 == 0 {
				sendSupportTicket()
			}
		}
	}()

	ticker := time.Tick(interval)

	for {

		// Get logs from the database
		logs, err := getLogs(db)
		if err != nil {
			errChan <- err
		}
		log.Println("Found", len(logs), "logs")

		// Send Email related to logs
		err = sendLogMail(logs, TOEMAIL)
		if err != nil {
			log.Println(err)
			errChan <- err
		}

		// Wait until the next interval to repeat
		<-ticker
	}
}

func openDB() (*sql.DB, error) {
	return sql.Open("mysql", "royalcoffee:agile123!@tcp(services.agilityintegrations.com)/moulton")
}

// getLogs queries the database for _ records and returns the in an array
func getLogs(db *sql.DB) ([]moultonlog, error) {

	logs := make([]moultonlog, 0)
	ct := time.Now().Add(time.Hour).Format("2006-01-02 15:04:05")

	qryTimeRange := interval * -1

	pt := time.Now().Add(qryTimeRange).Format("2006-01-02 15:04:05")

	query := fmt.Sprintf("SELECT * FROM moultonlog WHERE moultonlog.insert_date > '%s' AND moultonlog.insert_date < '%s' AND error_msg != 'Success'", pt, ct)
	log.Println("Query:", query)

	rows, err := db.Query(query)
	if err != nil {
		return logs, err
	}
	defer rows.Close()

	for rows.Next() {

		ml := moultonlog{}

		err := rows.Scan(
			&ml.id,
			&ml.direction,
			&ml.errorCode,

			&ml.errorMsg,
			&ml.insertDate,
			&ml.invoiceID,
			&ml.isDeleted,
			&ml.isSync,
			&ml.mid,
			&ml.reqdata,
			&ml.respdata,
			&ml.status,
			&ml.udata,
			&ml.trackingNumber,
		)

		if err != nil {
			return logs, err
		}
		logs = append(logs, ml)
	}

	return logs, err
}

func sendLogMail(logs []moultonlog, toEmail string) error {
	if len(logs) == 0 {
		log.Println("No logs to send")
		return nil
	}
	if toEmail == "" {
		toEmail = "lw@defenage.com" // CLIENT EMAIL
	}
	if EMAILPW == "" {
		EMAILPW = "Ag3leBTS.com"
	}
	auth := smtp.PlainAuth("", "accounts@agilebts.com", EMAILPW, "smtp.gmail.com")
	to := []string{toEmail, "dhrumild@zbrains.net"}
	today := time.Now().Format("01-02-2006 03:04")
	msg := "To:" + toEmail + "\r\n" +
		"Subject: Moulton Error Logs " + today + "\r\n" +
		"\r\n" + fmt.Sprintf("%s", moultonlogs(logs).String())

	log.Println("Auth: ", auth)
	if err := smtp.SendMail("smtp.gmail.com:587", auth, "accounts@agilebts.com", to, []byte(msg)); err != nil {
		log.Println("Trouble sending email:", err.Error())
		return err
	}
	return nil
}

// String formats logs to string
func (mls moultonlogs) String() string {
	if len(mls) == 0 {
		return ""
	}
	var mlogs strings.Builder
	mlogs.WriteString("Invoice ID     -        Moulton ID        -     Error Message\n\n")
	for _, m := range mls {
		fmt.Fprintf(&mlogs, "%s - %s - %s \n\n", m.invoiceID, m.mid, m.errorMsg)
	}
	return mlogs.String()
}

// moultonlog -- database row from the logs table
type moultonlog struct {
	id             int
	direction      string
	errorCode      int
	errorMsg       string
	insertDate     string
	invoiceID      string
	isDeleted      string
	isSync         string
	mid            string
	reqdata        string
	respdata       string
	status         string
	udata          sql.NullString
	trackingNumber sql.NullString
}

type moultonlogs []moultonlog

func setLogFile() *os.File {
	today := time.Now().Format("01022006")
	f, err := os.OpenFile("logs/"+today+"moulton.log", os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		panic(err)
	}
	logger := log.New(f, "", log.Lshortfile|log.Ldate|log.Ltime)
	log.SetOutput(io.MultiWriter(logger.Writer(), os.Stdout))
	return f
}

func sendSupportTicket() {
	toEmail := "support@zbrains.net"
	auth := smtp.PlainAuth("", "Accounts@agilebts.com", "Ag3leBTS.com", "smtp.gmail.com")
	to := []string{toEmail}
	msg := "To:" + toEmail + "\r\n" +
		"Subject: Check Moulton Logs App\r\n" +
		"\r\nPlease check the logs for the Moulton log application\n"
	smtp.SendMail("smtp.gmail.com:587", auth, "accounts@agilebts.com", to, []byte(msg))
}
