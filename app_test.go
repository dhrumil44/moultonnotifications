package main

import (
	"fmt"
	"testing"
)

func TestSendMail(t *testing.T) {
	tlog := moultonlog{id: 0, errorMsg: "hello"}
	tlog2 := moultonlog{id: 1, errorMsg: "world"}

	logs := []moultonlog{tlog, tlog2}
	err := sendLogMail(logs, "noahj@zbrains.net")
	if err != nil {
		t.Fatal(err)
	}
}

func TestGetLogs(t *testing.T) {
	db, _ := openDB()
	logs, err := getLogs(db)
	if err != nil {
		t.Fatal(err)
	}
	fmt.Printf("Got %d logs\n", len(logs))
}

func TestOpenDB(t *testing.T) {
	db, err := openDB()
	if err != nil {
		t.Fatal(err)
	}
	err = db.Ping()
	if err != nil {
		t.Fatal(err)
	}
	db.Close()
}
