FROM golang:1.12.9-alpine AS builder

###
# Builder 
###

# Download and install system and dependencies
RUN  \ 
    mkdir -p /zbrains.net/defenage/moulton-notifications && \
    apk add git && \
    apk update && \
    apk --no-cache add ca-certificates

WORKDIR /zbrains.net/defenage/moulton-notifications
COPY . .
# Build Application
ENV CGO_ENABLED=0
RUN go mod download
RUN go mod verify
RUN go build -o moulton-notifications

###
# Scratch
###
FROM scratch

WORKDIR /


# Copy binary and static assets
COPY --from=builder /zbrains.net/defenage/moulton-notifications .
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
ENTRYPOINT ["./moulton-notifications"]

